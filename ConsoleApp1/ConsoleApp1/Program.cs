﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[5] { 3,1,2,4,5};
            List<int> listings = new List<int>();
            listings.Add(3);
            listings.Add(1);
            listings.Add(2);
            listings.Add(4);
            listings.Add(5);
            int ans = countPreSorted(listings);
            Console.WriteLine(ans);
        }

        private static int countPreSorted(List<int> arr)
        {
            int[] arrs = arr.ToArray();
            int n = arrs.Length;
            int Unchanged = 0;
            int[] b = new int[n];
            for (int i = 0; i < n; i++)
            {
                b[i] = arrs[i];
            }
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (b[j] > b[j + 1])
                    {
                        int temp = b[j];
                        b[j] = b[j + 1];
                        b[j + 1] = temp;
                    }
                }
            }
            for (int i = 0; i < n; i++)
            {
                if (arrs[i] == b[i])
                {
                    Unchanged++;
                }
            }
            return Unchanged;
        }

/*        private static int calculate(int[] arr)
        {
            int n = arr.Length;
            int Unchanged = 0;
            int[] b = new int[n];
            for (int i = 0; i < n; i++)
            {
                b[i] = arr[i];
            }
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n - i - 1; j++)
                {
                    if (b[j] > b[j + 1])
                    {
                        int temp = b[j];
                        b[j] = b[j + 1];
                        b[j + 1] = temp;
                    }
                }
            }
            for (int i=0;i<n;i++)
            {
                if(arr[i]==b[i])
                {
                    Unchanged++;
                }
            }
            return Unchanged;
        }
*/    }
}
