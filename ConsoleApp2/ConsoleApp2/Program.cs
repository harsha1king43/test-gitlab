﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter Password 1 : ");
            long password1 = Convert.ToInt64(Console.ReadLine());
            Console.WriteLine("Enter Password 2 : ");
            long password2 = Convert.ToInt64(Console.ReadLine());
            int[] arr1 = new int[password1.ToString().Length];
            int[] arr2 = new int[password2.ToString().Length];
            int[] SolArr = new int[password2.ToString().Length];
            int index = 0;
            arr1 = fillingArray(password1,arr1);
            arr2 = fillingArray(password2,arr2);
            for(int i=0;i<password2.ToString().Length;i++)
            {
                for(int j=0;j< password1.ToString().Length;j++)
                {
                    if(arr2[i].Equals(arr1[j]))
                    {
                        arr1[j] = Convert.ToInt32(null);
                    }
                }
            }
            for(int i=0;i<arr1.Length;i++)
            {
                if(arr1[i] != (Convert.ToInt32(null)))
                {
                    SolArr[index] = arr1[i];
                    index++;
                }
            }
            for(int i=0;i<SolArr.Length;i++)
            {
                Console.WriteLine(SolArr[i]);
            }
        }

        private static int[] fillingArray(long password, int[] arr1)
        {
            for (int i = 0; i < password.ToString().Length; i++)
            {
                long r = password % 10;
                password = password / 10;
                arr1[password.ToString().Length - i] = Convert.ToInt32(r);
            }
            return arr1;
        }
    }
}
