﻿using System;

namespace NthLargestElement
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[] {5,7,12,23,48,17 };
            Console.WriteLine("Enter the location");
            int location = Convert.ToInt32(Console.ReadLine());
            for(int i=0;i<arr.Length;i++)
            {
                for(int j=0;j<arr.Length-i;j++)
                {
                    if(arr[j]>arr[j+1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            int position = arr.Length - location;
            Console.WriteLine("The "+location+"th largest element in an array is : "+arr[position]);
        }
    }
}
