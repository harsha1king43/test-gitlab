﻿using System;

namespace BasicProg1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            do
            {
                displayMenu();
                int choice = Convert.ToInt32(Console.ReadLine());
                switch(choice)
                {
                    case 1:
                        //Completed
                        CountingInduvidualNumbers();
                        EndOfProgram();
                        break;
                    case 2:
                        //Completed
                        NthLargestElement();
                        EndOfProgram();
                        break;
                    
                    

                   
                    default:
                        flag = false;
                        break;
                }

            } while (flag);
            
        }

        public int totalTurns(string input1, int input2, int input3)
        {
            string rotationalInput = input1 + input1;
            char[] ch = rotationalInput.ToCharArray();
            string refer = "";
            for(int i= input1.Length-1; i<input1.Length+i;i++)
            {
                /*if(input1.Length + i >= ch.Length)
                {
                    break;
                }
                else
                {*/
                    refer = refer + ch[i];
                    Console.Write(refer);
                    
                //}
                Console.WriteLine();
            }
            return 0;
        }

        private static void LetterReplacementsAndAddition()
        {
            Console.WriteLine("Enter String 1");
            string word1 = Console.ReadLine();
            Console.WriteLine("Enter String 2");
            string word2 = Console.ReadLine();
            Console.WriteLine("Enter Length N");
            int num = Convert.ToInt32(Console.ReadLine());
            int EndLength = word1.Length + word2.Length;
            char[] wordOne = word1.ToCharArray();
            char[] wordTwo = word2.ToCharArray();
            string Answer = "";
            int flag = 1;
            int run = 0;
            int stWord = 0; 
            int ndWord = 0; 
            for(int i=0;i<EndLength;i++)
            {
                if(flag==1)
                {
                    if(stWord < word1.Length)
                    {
                        Answer = Answer + wordOne[stWord];
                        stWord++;
                    }
                    run++;
                    if(run == num)
                    {
                        flag = 0;
                        run = 0;
                    }
                }
                else
                {
                    if (ndWord < word2.Length)
                    {
                        Answer = Answer + wordTwo[ndWord];
                        ndWord++;
                    }
                    run++;
                    if (run == num)
                    {
                        flag = 1;
                        run = 0;
                    }
                }
            }
            Console.WriteLine(Answer);

        }

        private static void AssendingFrequencyWords()
        {
            //Input -> tweet
            //Output -> tteew
            Console.WriteLine("Enter a word");
            string word = Console.ReadLine();
            char[] ch = word.ToCharArray();
            char[] tempCh = new char[ch.Length];
            int[] arr = new int[ch.Length];
            for(int i=0;i<ch.Length-1;i++)
            {
                int freq = 1;
                char letter = ch[i];
                for (int j=i+1;j<ch.Length;j++)
                {
                    
                    if(ch[i].Equals(ch[j]))
                    {
                        freq++;
                        ch[j] = ' ';
                    }
                }
                //tempCh[freq] = ;
            }
        }

        private static void EndOfProgram()
        {
            Console.WriteLine(" ");
            Console.WriteLine(" ");
        }

        private static void NthLargestElement()
        {
            int[] arr = new int[] { 5, 7, 12, 23, 48, 17 };
            Console.WriteLine("Enter the location");
            int location = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length - i -1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            int position = arr.Length - location;
            Console.WriteLine("The " + location + "th largest element in an array is : " + arr[position]);
        }

        private static void CountingInduvidualNumbers()
        {
            int sum = 0;
            Console.WriteLine("Give a number.");
            int number = Convert.ToInt32(Console.ReadLine());
            int numbers = number;
            int len = 1;
            while (numbers > 9)
            {
                numbers = numbers / 10;
                len++;
            }
            for (int i = 0; i < len; i++)
            {
                int last = number % 10;
                number = number / 10;
                sum = sum + last;
            }
            Console.WriteLine("Ans : " + sum);
        }

        private static void displayMenu()
        {
            Console.WriteLine("***********************************************");
            Console.WriteLine("Enter 1 for Counting Induvidual numbers in a number");
            Console.WriteLine("Enter 2 to find the Nth largest number in an array");
            Console.WriteLine("Enter 3 to get the alphabets in a word in assesnding order accounding to frequency");
            Console.WriteLine("Enter 4 for Sai Kumar Question");
            Console.WriteLine("Enter 5 for Raji Question");
            Console.WriteLine("Enter any other key to exit.");
            Console.WriteLine("***********************************************");
        }
    }
}